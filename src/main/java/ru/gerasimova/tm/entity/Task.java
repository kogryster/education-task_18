package ru.gerasimova.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractEntity implements Serializable {

//public  final class Task implements Serializable {

    public static final long serialVersionUID = 1;

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getId() + ": " + getName();
    }
}
