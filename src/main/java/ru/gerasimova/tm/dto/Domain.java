package ru.gerasimova.tm.dto;

import ru.gerasimova.tm.entity.Project;
import ru.gerasimova.tm.entity.Task;
import ru.gerasimova.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//@JsonIgnorePropertis(ignoreUnknown = true)
public final class Domain implements Serializable {

    private List<Project> projects = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<User> users = new ArrayList<>();

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
