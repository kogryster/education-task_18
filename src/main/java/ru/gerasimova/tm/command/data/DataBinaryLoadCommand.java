package ru.gerasimova.tm.command.data;

import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Load data from binary file. (Load).";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();

        setDomain(domain);

        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT OK]");

        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

}
