package ru.gerasimova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;


public final class DataJsonSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-save";
    }

    @Override
    public String description() {
        return "Save data by json.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");

        final Domain domain = getDomain();

        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile((file.toPath()));

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        System.out.println(json);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
        System.out.println();
    }

}
