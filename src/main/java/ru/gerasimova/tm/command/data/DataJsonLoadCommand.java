package ru.gerasimova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String description() {
        return "Load data from json file. (Load).";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");

        String json = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON)));
        final ObjectMapper mapper = new ObjectMapper();
        final Domain domain = mapper.readValue(json, Domain.class);

        setDomain(domain);

        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT OK]");
        System.out.println("[OK]");
    }

}
