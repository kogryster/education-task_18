package ru.gerasimova.tm.command.data;

import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;


public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-b64-save";
    }

    @Override
    public String description() {
        return "Save data by B64. (Save b64).";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA B64 SAVE]");

        final Domain domain = getDomain();

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final String base64 = new BASE64Encoder().encode(bytes);

        final File file = new File(DataConstant.FILE64_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile((file.toPath()));

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());

        fileOutputStream.close();

        System.out.println("[OK]");
        System.out.println();
    }

}
