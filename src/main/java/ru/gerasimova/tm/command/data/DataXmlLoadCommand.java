package ru.gerasimova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    public String description() {
        return "Load data from xml file. (Load).";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");


        String xml = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML)));
        final ObjectMapper xmlMapper = new XmlMapper();
        final Domain domain = xmlMapper.readValue(xml, Domain.class);

        setDomain(domain);

        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT OK]");
        System.out.println("[OK]");
    }

}
