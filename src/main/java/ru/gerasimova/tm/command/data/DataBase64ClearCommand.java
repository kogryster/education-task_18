package ru.gerasimova.tm.command.data;

import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;

import java.io.File;
import java.nio.file.Files;

public final class DataBase64ClearCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-b64-clear";
    }

    @Override
    public String description() {
        return "Remove data B64. (Clear B64).";
    }

    @Override
    public void execute() throws Exception {
        final File file = new File(DataConstant.FILE64_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
        System.out.println();
    }

}
