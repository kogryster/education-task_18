package ru.gerasimova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;


public final class DataXmlSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-xml-save";
    }

    @Override
    public String description() {
        return "Save data by xml.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");

        final Domain domain = getDomain();

        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile((file.toPath()));

        final ObjectMapper objectMapper = new XmlMapper();
        final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        System.out.println(xml);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
        System.out.println();
    }

}
