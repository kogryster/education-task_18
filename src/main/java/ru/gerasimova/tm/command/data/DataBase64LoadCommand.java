package ru.gerasimova.tm.command.data;

import ru.gerasimova.tm.command.data.AbstractDataCommand;
import ru.gerasimova.tm.constant.DataConstant;
import ru.gerasimova.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-b64-load";
    }

    @Override
    public String description() {
        return "Load data from b64 file. (Load).";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");

        String objectInputStreamString = new String(Files.readAllBytes(Paths.get(DataConstant.FILE64_BINARY)));
        byte[] bytes64 = new BASE64Decoder().decodeBuffer(objectInputStreamString);

        final ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes64);
        final ObjectInputStream objectInputStream2 = new ObjectInputStream(byteInputStream);

        final Domain domain = (Domain) objectInputStream2.readObject();

        setDomain(domain);

        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT OK]");

        objectInputStream2.close();
        System.out.println("[OK]");
    }

}
