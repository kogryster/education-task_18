package ru.gerasimova.tm.command.project;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.entity.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public void showProjectInfo(final Project project) {

        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

}
