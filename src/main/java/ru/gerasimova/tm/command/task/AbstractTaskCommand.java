package ru.gerasimova.tm.command.task;

import ru.gerasimova.tm.command.AbstractCommand;
import ru.gerasimova.tm.entity.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void showTaskInfo(final Task task) {

        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

}
