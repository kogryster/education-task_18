package ru.gerasimova.tm.exception.empty;

public class EmptyPasswordException extends AbstractEmptyException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
