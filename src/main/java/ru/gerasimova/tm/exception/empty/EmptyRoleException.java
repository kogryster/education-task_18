package ru.gerasimova.tm.exception.empty;

public class EmptyRoleException extends AbstractEmptyException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}
