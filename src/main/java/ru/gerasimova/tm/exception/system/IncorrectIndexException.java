package ru.gerasimova.tm.exception.system;

public class IncorrectIndexException extends AbstractSystemException {

    public IncorrectIndexException(Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(String value) {
        super("Error! This ``" + value + "`` is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrected...");
    }

}
