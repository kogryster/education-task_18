package ru.gerasimova.tm.exception.system;

public class IncorrectArgumentException extends AbstractSystemException {

    public IncorrectArgumentException(String value) {
        super("Error! Argument ``" + value + "`` not supported...");
    }

}
