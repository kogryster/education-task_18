package ru.gerasimova.tm.api.service;

import ru.gerasimova.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
