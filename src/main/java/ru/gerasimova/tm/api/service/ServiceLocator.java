package ru.gerasimova.tm.api.service;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthenticationService getAuthService();

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IDomainService getDomainService();

}
