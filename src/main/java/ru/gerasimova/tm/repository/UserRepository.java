package ru.gerasimova.tm.repository;

import ru.gerasimova.tm.api.repository.IUserRepository;
import ru.gerasimova.tm.entity.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        final List<User> result = new ArrayList<>();
        for (final User user : users) {
            result.add(user);
        }
        return result;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public void addAll(Collection<User> users) {
        if (users == null) return;
        this.users.addAll(users);
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User updateUserPasswordHash(final String id, final String passwordHash) {
        final User user = findById(id);
        user.setPasswordHash(passwordHash);
        return null;
    }

    @Override
    public void load(Collection<User> users) {
        clear();
        merge(users);
    }

    @Override
    public void merge(User... users) {
        for (final User user : users) merge(user);
    }

    @Override
    public void merge(Collection<User> users) {
        for (final User user : users) merge(user);
    }

    @Override
    public User merge(User user) {
        if (!users.contains(user)) users.add(user);
        return user;
    }

    @Override
    public void clear() {
        users.clear();
    }

}
