package ru.gerasimova.tm.service;

import ru.gerasimova.tm.api.service.IDomainService;
import ru.gerasimova.tm.api.service.ServiceLocator;
import ru.gerasimova.tm.dto.Domain;

public final class DomainService implements IDomainService {

    private final ServiceLocator serviceLocator;

    public DomainService(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
    }

}
